pragma solidity ^0.5.0;

contract Election {
	
	// This is how a candidate will look like.
	// Its like a template for the candidate
	struct Candidate {
		uint id;
		string name;
		uint voteCount;
	}

	// Associative array or a map.
	// The key will be a number (index) and the value will be a Candidate object
	// This is where Candidates are stored.
	mapping(uint => Candidate) public candidates;

	// A map to keep track of who has voted.
	mapping(address => bool) public voters;

	// Event to trigger when a vote is cast.
	event votedEvent (
		uint indexed _candidateId
	);

	// Number of candidates.
	uint public candidatesCount;

	constructor() public {
		addCandidate("Candidate 1");
		addCandidate("Candidate 2");
	}

	function addCandidate(string memory _name) private {
		candidatesCount++;
		candidates[candidatesCount] = Candidate(candidatesCount,_name,0);
	}

	function vote (uint _candidateId) public {
		// Require that the address has not voted before
		require(!voters[msg.sender]);

		// Require that the candidate is valid
		require (_candidateId >0 && _candidateId <= candidatesCount);
		

		// Record the fact that the voter has voted
		voters[msg.sender] = true;

		// Increment vote count for the candidate
		candidates[_candidateId].voteCount++;

		// trigger voted event
		emit votedEvent(_candidateId);
	}
}